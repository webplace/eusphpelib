# WebPlace/EusphpeLib

Library provides decrypt, signing methods for Ukrainian EDS.

## Installation

composer require `webplace/eusphpelib`

## Requirements
You need to download and install PHP extension [EUSPHPE](https://iit.com.ua). Installation manual is located in

- root
    - EUSPHPE
        - EUSignPHPDescription.doc

### Short manual for PHP-FPM (PHP v. 8.0)
1. Create a directory - **/usr/lib/php/8.0/eusphpe_extension**
2. Unpack [downloaded archive](https://iit.com.ua) to **/usr/lib/php/8.0/eusphpe_extension** directory | *Pick only needed file (archive/Modules/Linux/64/eusphpei.64.8.0.3.tar)*
3. Add to **/usr/lib/systemd/system/php8.0-fpm.service**  this line of code \
   **`export LD_LIBRARY_PATH=/usr/lib/php/8.0/eusphpe_extension`**
4. Create a new file  **/etc/php/8.0/fpm/conf.d/eusphpe.ini**, to this file add \
   **`extension=/usr/lib/php/8.0/eusphpe_extension/eusphpe.so`**
5. Create a new directory for certificates (for example */var/certificates*)
6. Open **`  /usr/lib/php/8.0/eusphpe_extension/osplm.ini`**	and  edit **Path** parameter  (for example */var/certificates (directory from previous step)*).
7. Upload your certificates in */var/certificates*
8. Restart FPM **`service php8.0-fpm restart`**

## Usage

```php
<?php
    use Webplace\EusphpeLib\MessageDecrypting;
    use Webplace\EusphpeLib\AsicSigning;

    $messageDecrypting = new MessageDecrypting('Key-6.dat', 'Password');

    return $messageDecrypting->decryptToString('EncryptedData', 'Cert');
    return $messageDecrypting->decrypt('EncryptedData', 'Cert')->toString();
    return $messageDecrypting->decrypt('EncryptedData', 'Cert')
        ->withVerify()
        ->verify()
        ->toString();

    $asicSigning = new AsicSigning('Key-6.dat', 'Password');

    return $asicSigning->signToString('Data to sign', 'SignedData.txt');
    return $asicSigning->sign('Data to sign', 'SignedData.txt')->toString();

    echo $asicSigning->sign('Data to sign', 'SignedData.txt')
        ->withVerify()
        ->verify()
        ->toString();

    return $asicSigning->getDriver()->toString();
```
## Versioning

Maintained under the [Semantic Versioning guidelines](https://semver.org/).

## Reference

* Key Certificate Authority user. Signature library https://iit.com.ua
* Example on https://github.com/andrew-svirin/eusphpe-php

[⬆ back to top](#Installation)
