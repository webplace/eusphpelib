<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use Webplace\EusphpeLib\Interfaces\DriverResult;
use Webplace\EusphpeLib\Traits\PropertyToString;

final class AsicSignVerifyInfo implements DriverResult
{
    use PropertyToString;

    public $signTime;
    public $useTSP;
    public $issuer;
    public $issuerCN;
    public $serial;
    public $subject;
    public $subjCN;
    public $subjOrg;
    public $subjOrgUnit;
    public $subjTitle;
    public $subjState;
    public $subjLocality;
    public $subjFullName;
    public $subjAddress;
    public $subjPhone;
    public $subjEMail;
    public $subjDNS;
    public $subjEDRPOUCode;
    public $subjDRFOCode;

    public function getData(): string
    {
        return $this->propertyToString();
    }
}
