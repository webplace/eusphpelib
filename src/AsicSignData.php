<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use Webplace\EusphpeLib\Interfaces\DriverResult;

final class AsicSignData implements DriverResult
{
    public $data;

    public function getData()
    {
        return $this->data;
    }
}
