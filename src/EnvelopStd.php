<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use stdClass;
use Webplace\EusphpeLib\Interfaces\DriverResult;

class EnvelopStd extends stdClass implements DriverResult
{
    public $data;
    public $signTime;
    public $useTSP;
    public $issuer;
    public $issuerCN;
    public $serial;
    public $subject;
    public $subjCN;
    public $subjOrg;
    public $subjOrgUnit;
    public $subjTitle;
    public $subjState;
    public $subjLocality;
    public $subjFullName;
    public $subjAddress;
    public $subjPhone;
    public $subjEMail;
    public $subjDNS;
    public $subjEDRPOUCode;
    public $subjDRFOCode;

    public function getData()
    {
        return $this->data;
    }
}
