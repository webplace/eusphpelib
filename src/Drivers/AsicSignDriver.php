<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Drivers;

use Webplace\EusphpeLib\AsicReference;
use Webplace\EusphpeLib\AsicSignData;
use Webplace\EusphpeLib\BaseDriver;
use Webplace\EusphpeLib\Enums\AsicType;
use Webplace\EusphpeLib\Enums\SignatureEnum;
use Webplace\EusphpeLib\Enums\SignTypeEnum;
use Webplace\EusphpeLib\Exceptions\LibException;
use Webplace\EusphpeLib\Exceptions\LibNotInitException;

class AsicSignDriver extends BaseDriver
{
    public int $signAlgo = SignatureEnum::EU_CTX_SIGN_ECDSA_WITH_SHA;
    public int $signType = SignTypeEnum::EU_ASIC_SIGN_TYPE_CADES;
    public int $signLevel = SignTypeEnum::EU_SIGN_TYPE_CADES_X_LONG;
    public int $asicType = AsicType::EU_ASIC_TYPE_E;
    protected ?AsicSignData $asicSignData = null;

    /**
     * @param AsicReference|AsicReference[] $containerReference
     *
     * @throws LibNotInitException
     * @throws LibException
     */
    public function sign($containerReference): self
    {
        if (empty($containerReference)) {
            throw new LibException('Required containerReferences parameter is not filled');
        }

        if (!is_array($containerReference)) {
            $containerReference = [$containerReference];
        }

        $referencesLabel = [];
        $referencesData = [];
        foreach ($containerReference as $reference) {
            if (!($reference instanceof AsicReference)) {
                throw new LibException('Parameter containerReferences in invalid');
            }

            $referencesLabel[] = $reference->getLabel();
            $referencesData[] = $reference->getData();
        }

        $this->checkInit();
        $this->asicSignData = new AsicSignData();
        $errorCode = 0;
        $this->verifyResult(
            euspe_ctxasicsigndata(
                $this->driverContext->keyContext,
                $this->signAlgo,
                $this->asicType,
                $this->signType,
                $this->signLevel,
                $referencesLabel,
                $referencesData,
                $this->asicSignData->data,
                $errorCode
            ),
            $errorCode
        );

        return $this;
    }

    /**
     * @throws LibNotInitException
     */
    public function withVerify($toVerify = null): AsicVerifyDriver
    {
        return AsicVerifyDriver::make($this->getContext())
            ->setToVerify($this->result()->getData() ?? $toVerify);
    }

    public function result(): ?AsicSignData
    {
        return $this->asicSignData;
    }

    public function toString(): string
    {
        return $this->asicSignData->data ? base64_encode($this->asicSignData->data) : '';
    }
}
