<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Drivers;

use Webplace\EusphpeLib\BaseDriver;
use Webplace\EusphpeLib\EnvelopStd;

class DecryptionDriver extends BaseDriver
{
    protected ?EnvelopStd $envelop = null;

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\LibNotInitException
     * @throws \Webplace\EusphpeLib\Exceptions\LibException
     */
    public function decryptMessage(string $message, string $cert): self
    {
        $this->checkInit();
        $this->envelop = new EnvelopStd();

        $errorCode = null;
        $this->verifyResult(
            euspe_ctxdevelopdata(
                $this->driverContext->keyContext,
                base64_decode($message),
                base64_decode($cert),
                $this->envelop->data,
                $this->envelop->signTime,
                $this->envelop->useTSP,
                $this->envelop->issuer,
                $this->envelop->issuerCN,
                $this->envelop->serial,
                $this->envelop->subject,
                $this->envelop->subjCN,
                $this->envelop->subjOrg,
                $this->envelop->subjOrgUnit,
                $this->envelop->subjTitle,
                $this->envelop->subjState,
                $this->envelop->subjLocality,
                $this->envelop->subjFullName,
                $this->envelop->subjAddress,
                $this->envelop->subjPhone,
                $this->envelop->subjEMail,
                $this->envelop->subjDNS,
                $this->envelop->subjEDRPOUCode,
                $this->envelop->subjDRFOCode,
                $errorCode
            ),
            $errorCode
        );

        return $this;
    }

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\LibNotInitException
     */
    public function withVerify($toVerify = null): DecryptionVerifyDriver
    {
        return DecryptionVerifyDriver::make($this->getContext())->setToVerify($this->result()->getData() ?? $toVerify);
    }

    public function result(): ?EnvelopStd
    {
        return $this->envelop;
    }

    public function toString(): string
    {
        return $this->envelop->data ?? '';
    }
}
