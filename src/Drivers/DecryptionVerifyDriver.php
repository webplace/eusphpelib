<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Drivers;

use Webplace\EusphpeLib\BaseDriver;
use Webplace\EusphpeLib\EnvelopStd;
use Webplace\EusphpeLib\Exceptions\LibException;

class DecryptionVerifyDriver extends BaseDriver
{
    public int $signIndex = 0;
    public $toVerify = null;

    protected bool $offlineMode = false;
    protected bool $noCrlMode = false;
    protected ?EnvelopStd $signInfo = null;

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\LibException
     * @throws \Webplace\EusphpeLib\Exceptions\LibNotInitException
     */
    public function verify($unverifiedData = null): self
    {
        $unverifiedData = $unverifiedData ?? $this->toVerify;

        if (empty($unverifiedData)) {
            throw new LibException('Set data to verification');
        }

        $this->checkInit();
        $this->signInfo = new EnvelopStd();
        $signTime = date('m.d.Y H:i:s');

        $errorCode = null;
        $this->verifyResult(
            euspe_signverifyontime(
                $this->signIndex,
                $unverifiedData,
                $signTime,
                $this->offlineMode,
                $this->noCrlMode,
                $this->signInfo->signTime,
                $this->signInfo->useTSP,
                $this->signInfo->issuer,
                $this->signInfo->issuerCN,
                $this->signInfo->serial,
                $this->signInfo->subject,
                $this->signInfo->subjCN,
                $this->signInfo->subjOrg,
                $this->signInfo->subjOrgUnit,
                $this->signInfo->subjTitle,
                $this->signInfo->subjState,
                $this->signInfo->subjLocality,
                $this->signInfo->subjFullName,
                $this->signInfo->subjAddress,
                $this->signInfo->subjPhone,
                $this->signInfo->subjEMail,
                $this->signInfo->subjDNS,
                $this->signInfo->subjEDRPOUCode,
                $this->signInfo->subjDRFOCode,
                $this->signInfo->data,
                $errorCode
            ),
            $errorCode
        );

        return $this;
    }

    public function setToVerify($toVerify): self
    {
        $this->toVerify = $toVerify;

        return $this;
    }

    public function offline(bool $value = true): self
    {
        $this->offlineMode = $value;

        return $this;
    }

    public function noCrl(bool $value = true): self
    {
        $this->noCrlMode = $value;

        return $this;
    }

    public function result(): ?EnvelopStd
    {
        return $this->signInfo;
    }

    public function toString(): string
    {
        return $this->signInfo->data ?? '';
    }
}
