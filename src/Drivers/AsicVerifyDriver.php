<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Drivers;

use Webplace\EusphpeLib\AsicSignVerifyInfo;
use Webplace\EusphpeLib\BaseDriver;
use Webplace\EusphpeLib\Exceptions\LibException;
use Webplace\EusphpeLib\Exceptions\LibNotInitException;

class AsicVerifyDriver extends BaseDriver
{
    public int $signIndex = 0;
    public ?string $toVerify = null;
    protected ?AsicSignVerifyInfo $verifyInfo = null;

    /**
     * @throws LibException
     * @throws LibNotInitException
     */
    public function verify($unverifiedData = null): self
    {
        $unverifiedData = $unverifiedData ?? $this->toVerify;

        if (empty($unverifiedData)) {
            throw new LibException('Set data to verification');
        }

        $this->checkInit();
        $this->verifyInfo = new AsicSignVerifyInfo();

        $errorCode = 0;
        $this->verifyResult(
            euspe_asicverifydata(
                $this->signIndex,
                $unverifiedData,
                $this->verifyInfo->signTime,
                $this->verifyInfo->useTSP,
                $this->verifyInfo->issuer,
                $this->verifyInfo->issuerCN,
                $this->verifyInfo->serial,
                $this->verifyInfo->subject,
                $this->verifyInfo->subjCN,
                $this->verifyInfo->subjOrg,
                $this->verifyInfo->subjOrgUnit,
                $this->verifyInfo->subjTitle,
                $this->verifyInfo->subjState,
                $this->verifyInfo->subjLocality,
                $this->verifyInfo->subjFullName,
                $this->verifyInfo->subjAddress,
                $this->verifyInfo->subjPhone,
                $this->verifyInfo->subjEMail,
                $this->verifyInfo->subjDNS,
                $this->verifyInfo->subjEDRPOUCode,
                $this->verifyInfo->subjDRFOCode,
                $errorCode
            ),
            $errorCode
        );

        return $this;
    }

    public function setToVerify($toVerify): self
    {
        $this->toVerify = $toVerify;
        return $this;
    }

    public function result(): ?AsicSignVerifyInfo
    {
        return $this->verifyInfo;
    }

    public function toString(): string
    {
        return $this->verifyInfo->getData();
    }
}
