<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use Webplace\EusphpeLib\Interfaces\DriverContext as DriverContextInterface;

class DriverContext implements DriverContextInterface
{
    /**
     * @var mixed
     */
    public $context = null;

    /**
     * @var mixed
     */
    public $keyContext = null;

    public ?KeyFile $keyFile = null;

    public function isInit(): bool
    {
        return $this->context && $this->keyContext && $this->keyFile;
    }

    public function isNotInit(): bool
    {
        return !$this->isInit();
    }

    public function free(bool $complete = true): void
    {
        $this->keyFile = null;

        if ($this->keyContext) {
            euspe_ctxfreeprivatekey($this->keyContext);
            $this->keyContext = null;
        }
        if ($this->context) {
            euspe_ctxfree($this->context);
            $this->context = null;
        }

        if ($complete) {
            euspe_finalize();
        }
    }
}
