<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use Webplace\EusphpeLib\Enums\KeyFileTypeEnum;
use Webplace\EusphpeLib\Exceptions\KeyFileException;
use Webplace\EusphpeLib\Interfaces\KeyFile as KeyFileInterface;

class KeyFile implements KeyFileInterface
{
    protected string $keyFilePath;

    /**
     * @var mixed
     */
    protected $fileContent;

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\KeyFileException
     */
    public function __construct(string $keyFilePath)
    {
        $this->read($keyFilePath);
    }

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\KeyFileException
     */
    public static function exists(string $keyFilePath): bool
    {
        if (empty($keyFilePath)) {
            throw new KeyFileException('Specify the path to the private key file', 1);
        }

        return file_exists($keyFilePath);
    }

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\KeyFileException
     */
    public function read(string $keyFilePath): void
    {
        $this->keyFilePath = $keyFilePath;

        if (!$this->exists($this->keyFilePath)) {
            throw new KeyFileException('Key file not found', 2, ['path' => $keyFilePath]);
        }

        $result = file_get_contents($this->keyFilePath);

        if ($result === false) {
            throw new KeyFileException('Key file invalid', 3, ['path' => $keyFilePath]);
        }

        $this->fileContent = $result;
    }

    public function type(): string
    {
        return KeyFileTypeEnum::DAT;
    }

    public function content(): string
    {
        return $this->fileContent;
    }

    public function getKeyFilePath(): string
    {
        return $this->keyFilePath;
    }
}
