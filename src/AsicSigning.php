<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use Webplace\EusphpeLib\Drivers\AsicSignDriver;
use Webplace\EusphpeLib\Exceptions\KeyFileException;
use Webplace\EusphpeLib\Exceptions\LibException;
use Webplace\EusphpeLib\Exceptions\LibNotInitException;

class AsicSigning
{
    protected AsicSignDriver $asicContainerDriver;

    /**
     * @throws LibException
     * @throws KeyFileException
     * @throws LibNotInitException
     */
    public function __construct(string $keyFilePath, string $password)
    {
        $this->asicContainerDriver = new AsicSignDriver();
        $this->asicContainerDriver->init();
        $this->asicContainerDriver->loadPrivateKey($keyFilePath, $password);
    }

    public function getDriver(): AsicSignDriver
    {
        return $this->asicContainerDriver;
    }

    /**
     * @param AsicReference|AsicReference[] $containerReference
     *
     * @throws LibNotInitException
     * @throws LibException
     */
    public function signCustom($containerReference): AsicSignDriver
    {
        return $this->getDriver()->sign($containerReference);
    }

    /**
     * @throws LibNotInitException
     * @throws LibException
     */
    public function sign(string $data, string $label): AsicSignDriver
    {
        return $this->signCustom(new AsicReference($data, $label));
    }

    /**
     * @throws LibNotInitException
     * @throws LibException
     */
    public function signToString(string $data, string $label): string
    {
        $signedData = $this->sign($data, $label);
        $signedData->withVerify()->verify();

        return $signedData->toString();
    }

    public function setSignAlgo(int $value)
    {
        $this->getDriver()->signAlgo = $value;
    }

    public function setSignType(int $value)
    {
        $this->getDriver()->signType = $value;
    }

    public function setSignLevel(int $value)
    {
        $this->getDriver()->signLevel = $value;
    }

    public function setAsicType(int $value)
    {
        $this->getDriver()->asicType = $value;
    }

    public function __destruct()
    {
        $this->getDriver()->free();
    }
}
