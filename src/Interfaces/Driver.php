<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Interfaces;

interface Driver
{
    public static function make(?DriverContext $driverContext = null): self;

    public function init(): void;

    public function loadPrivateKey(string $keyFilePath, string $password): void;

    public function getContext(): DriverContext;

    public function setContext(DriverContext $driverContext): void;

    public function checkInit(): void;

    public function free(): void;

    public function toString(): string;
}
