<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Interfaces;

interface DriverResult
{
    public function getData();
}
