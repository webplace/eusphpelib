<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Interfaces;

interface DriverContext
{
    public function isInit(): bool;

    public function isNotInit(): bool;

    public function free(bool $complete = true): void;
}
