<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Interfaces;

interface KeyFile
{
    public function read(string $keyFilePath): void;

    public function type(): string;

    public static function exists(string $keyFilePath): bool;

    public function content();
}
