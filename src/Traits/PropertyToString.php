<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Traits;

trait PropertyToString
{
    protected function propertyToString(): string
    {
        $result = '';
        foreach (get_object_vars($this) as $name => $value) {
            $result .= $name . ': ' . $value . PHP_EOL;
        }

        return $result;
    }
}
