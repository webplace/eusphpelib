<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Exceptions;

use Throwable;

final class KeyFileException extends AbstractException
{
    public function __construct($message = "", $code = 0, array $details = [], Throwable $previous = null)
    {
        if (!empty($details['path'])) {
            $message .= ' in path: ' . $details['path'];
        }

        parent::__construct($message, $code, $details, $previous);
    }
}
