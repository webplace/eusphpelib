<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Exceptions;

use Throwable;

abstract class AbstractException extends \Exception
{
    protected array $details = [];

    public function __construct($message = "", $code = 0, array $details = [], Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->details = $details;
    }

    /**
     * @return array
     */
    public function getDetails(): array
    {
        return $this->details;
    }
}
