<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Exceptions;

final class DecryptionException extends LibException
{
}
