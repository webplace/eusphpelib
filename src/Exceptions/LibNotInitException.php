<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Exceptions;

use Throwable;

class LibNotInitException extends AbstractException
{
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('Library no initiated. Call "init" method before', 0, [], $previous);
    }
}
