<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib\Exceptions;

use Throwable;

class LibException extends AbstractException
{
    protected string $description = '';

    public function __construct($message = "", $code = 0, array $details = [], Throwable $previous = null)
    {
        parent::__construct($message, $code, $details, $previous);

        try {
            euspe_geterrdescr($this->code, $this->description);
        } catch (Throwable $e) {
        }

        $this->message = $message . " ({$this->codeToHex()}) " . $this->description;
    }

    public function codeToHex(): string
    {
        return '0x' . str_pad(dechex($this->code), 4, '0', STR_PAD_LEFT);
    }
}
