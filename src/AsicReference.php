<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

class AsicReference
{
    protected string $data;
    protected string $label;

    public function __construct(string $data, string $label)
    {
        $this->data = $data;
        $this->label = $label;
    }

    public static function fromArray(array $data): array
    {
        $result = [];
        foreach ($data as $name => $value) {
            $result[] = new static($name, $value);
        }

        return $result;
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
