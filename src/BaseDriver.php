<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use Webplace\EusphpeLib\Enums\EncodingEnum;
use Webplace\EusphpeLib\Enums\ErrorCodeEnum;
use Webplace\EusphpeLib\Exceptions\KeyFileException;
use Webplace\EusphpeLib\Exceptions\LibException;
use Webplace\EusphpeLib\Exceptions\LibNotInitException;
use Webplace\EusphpeLib\Interfaces\Driver;
use Webplace\EusphpeLib\Interfaces\DriverContext as DriverContextInterface;

abstract class BaseDriver implements Driver
{
    protected DriverContextInterface $driverContext;

    public function __construct(?DriverContextInterface $driverContext = null)
    {
        if (is_null($driverContext)) {
            $this->driverContext = new DriverContext();
        } else {
            $this->setContext($driverContext);
        }
    }

    public static function make(?DriverContextInterface $driverContext = null): self
    {
        return new static($driverContext);
    }

    /**
     * @throws LibException
     */
    public function init(): void
    {
        $this->driverContext = new DriverContext();

        euspe_setcharset(EncodingEnum::EM_ENCODING_UTF8);

        $errorCode = null;
        $this->verifyResult(euspe_init($errorCode), $errorCode);
        $this->verifyResult(euspe_ctxcreate($this->driverContext->context, $errorCode), $errorCode);
    }

    /**
     * @throws KeyFileException
     * @throws LibException
     * @throws LibNotInitException
     */
    public function loadPrivateKey(string $keyFilePath, string $password): void
    {
        if (!$this->driverContext->context) {
            throw new LibNotInitException();
        }

        $this->driverContext->keyFile = new KeyFile($keyFilePath);

        $errorCode = null;
        $this->verifyResult(
            euspe_ctxreadprivatekeybinary(
                $this->driverContext->context,
                $this->driverContext->keyFile->content(),
                $password,
                $this->driverContext->keyContext,
                $errorCode
            ),
            $errorCode
        );
    }

    /**
     * @throws LibNotInitException
     */
    public function getContext(): DriverContextInterface
    {
        $this->checkInit();

        return $this->driverContext;
    }

    public function setContext(DriverContextInterface $driverContext): void
    {
        $this->driverContext = $driverContext;
    }

    /**
     * @throws LibNotInitException
     */
    public function checkInit(): void
    {
        if ($this->driverContext->isNotInit()) {
            throw new LibNotInitException();
        }
    }

    public function free(): void
    {
        $this->driverContext->free();
    }

    /**
     * @throws LibException
     */
    protected function verifyResult($result, $errorCode): void
    {
        $errorCode = $errorCode ?? ErrorCodeEnum::EU_ERROR_UNKNOWN;
        switch ($result) {
            case 1:
                throw new LibException('Operation error', $errorCode);
            case 2:
                throw new LibException('Operation error: Wrong params', $errorCode);
            case 3:
                throw new LibException('Operation error: Cipher initialization error', $errorCode);
        }
    }

    public function toString(): string
    {
        return '';
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
