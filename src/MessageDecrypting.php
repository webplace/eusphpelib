<?php

declare(strict_types=1);

namespace Webplace\EusphpeLib;

use Webplace\EusphpeLib\Drivers\DecryptionDriver;

class MessageDecrypting
{
    protected DecryptionDriver $decryptionDriver;

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\LibException
     * @throws \Webplace\EusphpeLib\Exceptions\KeyFileException
     * @throws \Webplace\EusphpeLib\Exceptions\LibException
     * @throws \Webplace\EusphpeLib\Exceptions\LibNotInitException
     */
    public function __construct(string $keyFilePath, string $password)
    {
        $this->decryptionDriver = new DecryptionDriver();
        $this->decryptionDriver->init();
        $this->decryptionDriver->loadPrivateKey($keyFilePath, $password);
    }

    public function getDriver(): DecryptionDriver
    {
        return $this->decryptionDriver;
    }

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\LibNotInitException
     * @throws \Webplace\EusphpeLib\Exceptions\LibException
     */
    public function decrypt(string $messageB64, string $certB64): DecryptionDriver
    {
        return $this->getDriver()->decryptMessage($messageB64, $certB64);
    }

    /**
     * @throws \Webplace\EusphpeLib\Exceptions\LibNotInitException
     * @throws \Webplace\EusphpeLib\Exceptions\LibException
     */
    public function decryptToString(string $messageB64, string $certB64): string
    {
        return $this->decrypt($messageB64, $certB64)
            ->withVerify()
            ->verify()
            ->toString();
    }

    public function __destruct()
    {
        $this->getDriver()->free();
    }
}
