<?php

namespace Webplace\EusphpeLib\Enums;

interface ParameterEnum
{
    public const EU_RESOLVE_OIDS_PARAMETER = "ResolveOIDs";
    public const EU_SIGN_INCLUDE_CONTENT_TIME_STAMP_PARAMETER = "SignIncludeContentTimeStamp";
    public const EU_SIGN_TYPE_PARAMETER = "SignType";
    public const EU_SIGN_INCLUDE_CA_CERTIFICATES_PARAMETER = "SignIncludeCACertificates";
}
