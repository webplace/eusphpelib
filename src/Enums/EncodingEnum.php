<?php

namespace Webplace\EusphpeLib\Enums;

interface EncodingEnum
{
    public const EM_ENCODING_CP1251 = 1251;
    public const EM_ENCODING_UTF8 = 65001;
}
