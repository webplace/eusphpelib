<?php

namespace Webplace\EusphpeLib\Enums;

interface KeyTypeEnum
{
    public const EU_CERT_KEY_TYPE_UNKNOWN = 0x00;
    public const EU_CERT_KEY_TYPE_DSTU4145 = 0x01;
    public const EU_CERT_KEY_TYPE_RSA = 0x02;
}
