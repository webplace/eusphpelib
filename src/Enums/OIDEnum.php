<?php

namespace Webplace\EusphpeLib\Enums;

interface OIDEnum
{
    public const EU_OID_EXT_KEY_USAGE_STAMP = "1.2.804.2.1.1.1.3.9";
}
