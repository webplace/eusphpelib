<?php

namespace Webplace\EusphpeLib\Enums;

interface ErrorCodeEnum
{
    public const EM_RESULT_OK = 0;
    public const EM_RESULT_ERROR = 1;
    public const EM_RESULT_ERROR_WRONG_PARAMS = 2;
    public const EM_RESULT_ERROR_INITIALIZED = 3;
    public const EU_ERROR_NONE = 0x0000;
    public const EU_ERROR_UNKNOWN = 0xFFFF;
    public const EU_ERROR_NOT_SUPPORTED = 0xFFFE;
    public const EU_ERROR_NOT_INITIALIZED = 0x0001;
    public const EU_ERROR_BAD_PARAMETER = 0x0002;
    public const EU_ERROR_LIBRARY_LOAD = 0x0003;
    public const EU_ERROR_READ_SETTINGS = 0x0004;
    public const EU_ERROR_TRANSMIT_REQUEST = 0x0005;
    public const EU_ERROR_MEMORY_ALLOCATION = 0x0006;
    public const EU_WARNING_END_OF_ENUM = 0x0007;
    public const EU_ERROR_PROXY_NOT_AUTHORIZED = 0x0008;
    public const EU_ERROR_NO_GUI_DIALOGS = 0x0009;
    public const EU_ERROR_DOWNLOAD_FILE = 0x000A;
    public const EU_ERROR_WRITE_SETTINGS = 0x000B;
    public const EU_ERROR_CANCELED_BY_GUI = 0x000C;
    public const EU_ERROR_OFFLINE_MODE = 0x000D;
    public const EU_ERROR_KEY_MEDIAS_FAILED = 0x0011;
    public const EU_ERROR_KEY_MEDIAS_ACCESS_FAILED = 0x0012;
    public const EU_ERROR_KEY_MEDIAS_READ_FAILED = 0x0013;
    public const EU_ERROR_KEY_MEDIAS_WRITE_FAILED = 0x0014;
    public const EU_WARNING_KEY_MEDIAS_READ_ONLY = 0x0015;
    public const EU_ERROR_KEY_MEDIAS_DELETE = 0x0016;
    public const EU_ERROR_KEY_MEDIAS_CLEAR = 0x0017;
    public const EU_ERROR_BAD_PRIVATE_KEY = 0x0018;
    public const EU_ERROR_PKI_FORMATS_FAILED = 0x0021;
    public const EU_ERROR_CSP_FAILED = 0x0022;
    public const EU_ERROR_BAD_SIGNATURE = 0x0023;
    public const EU_ERROR_AUTH_FAILED = 0x0024;
    public const EU_ERROR_NOT_RECEIVER = 0x0025;
    public const EU_ERROR_STORAGE_FAILED = 0x0031;
    public const EU_ERROR_BAD_CERT = 0x0032;
    public const EU_ERROR_CERT_NOT_FOUND = 0x0033;
    public const EU_ERROR_INVALID_CERT_TIME = 0x0034;
    public const EU_ERROR_CERT_IN_CRL = 0x0035;
    public const EU_ERROR_BAD_CRL = 0x0036;
    public const EU_ERROR_NO_VALID_CRLS = 0x0037;
    public const EU_ERROR_GET_TIME_STAMP = 0x0041;
    public const EU_ERROR_BAD_TSP_RESPONSE = 0x0042;
    public const EU_ERROR_TSP_SERVER_CERT_NOT_FOUND = 0x0043;
    public const EU_ERROR_TSP_SERVER_CERT_INVALID = 0x0044;
    public const EU_ERROR_GET_OCSP_STATUS = 0x0051;
    public const EU_ERROR_BAD_OCSP_RESPONSE = 0x0052;
    public const EU_ERROR_CERT_BAD_BY_OCSP = 0x0053;
    public const EU_ERROR_OCSP_SERVER_CERT_NOT_FOUND = 0x0054;
    public const EU_ERROR_OCSP_SERVER_CERT_INVALID = 0x0055;
    public const EU_ERROR_LDAP_ERROR = 0x0061;
}
