<?php

namespace Webplace\EusphpeLib\Enums;

interface RecipientEnum
{
    public const EU_RECIPIENT_APPEND_TYPE_BY_ISSUER_SERIAL = 0x01;
    public const EU_RECIPIENT_APPEND_TYPE_BY_KEY_ID = 0x02;
}
