<?php

namespace Webplace\EusphpeLib\Enums;

interface SignTypeEnum
{
    public const EU_SIGN_TYPE_UNKNOWN = 0x00;
    public const EU_SIGN_TYPE_CADES_BES = 0x01;
    public const EU_SIGN_TYPE_CADES_T = 0x04;
    public const EU_SIGN_TYPE_CADES_C = 0x08;
    public const EU_SIGN_TYPE_CADES_X_LONG = 0x10;
    /** @deprecated @see AsicType */
    public const EU_ASIC_TYPE_UNKNOWN = 0;
    /** @deprecated @see AsicType */
    public const EU_ASIC_TYPE_S = 1;
    /** @deprecated @see AsicType */
    public const EU_ASIC_TYPE_E = 2;
    public const EU_ASIC_SIGN_TYPE_UNKNOWN = 0;
    public const EU_ASIC_SIGN_TYPE_CADES = 1;
    public const EU_ASIC_SIGN_TYPE_XADES = 2;
}
