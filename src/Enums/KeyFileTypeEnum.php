<?php

namespace Webplace\EusphpeLib\Enums;

interface KeyFileTypeEnum
{
    public const DAT = 'dat';
    public const JKS = 'jks';
}
