<?php

namespace Webplace\EusphpeLib\Enums;

interface KeyUsageEnum
{
    public const EU_KEY_USAGE_UNKNOWN = 0x0000;
    public const EU_KEY_USAGE_DIGITAL_SIGNATURE = 0x0001;
    public const EU_KEY_USAGE_KEY_AGREEMENT = 0x0010;
}
