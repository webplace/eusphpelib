<?php

namespace Webplace\EusphpeLib\Enums;

interface ContextEnum
{
    public const EU_CTX_HASH_ALGO_UNKNOWN = 0x00;
    public const EU_CTX_HASH_ALGO_GOST34311 = 0x01;
    public const EU_CTX_HASH_ALGO_SHA160 = 0x02;
    public const EU_CTX_HASH_ALGO_SHA224 = 0x03;
    public const EU_CTX_HASH_ALGO_SHA256 = 0x04;
    public const EU_CTX_SIGN_UNKNOWN = 0x00;
    public const EU_CTX_SIGN_DSTU4145_WITH_GOST34311 = 0x01;
    public const EU_CTX_SIGN_RSA_WITH_SHA = 0x02;
}
