<?php

namespace Webplace\EusphpeLib\Enums;

interface SignatureEnum
{
    public const EU_CTX_SIGN_UNKNOWN = 0x00;
    public const EU_CTX_SIGN_DSTU4145_WITH_GOST34311 = 0x01;
    public const EU_CTX_SIGN_RSA_WITH_SHA = 0x02;
    public const EU_CTX_SIGN_ECDSA_WITH_SHA = 0x03;
    public const EU_CTX_SIGN_DSTU4145_WITH_DSTU7564 = 0x04;
}
