<?php

namespace Webplace\EusphpeLib\Enums;

interface SubjectEnum
{
    public const EU_SUBJECT_TYPE_UNDIFFERENCED = 0;
    public const EU_SUBJECT_TYPE_CA = 1;
    public const EU_SUBJECT_TYPE_CA_SERVER = 2;
    public const EU_SUBJECT_TYPE_RA_ADMINISTRATOR = 3;
    public const EU_SUBJECT_TYPE_END_USER = 4;
    public const EU_SUBJECT_CA_SERVER_SUB_TYPE_UNDIFFERENCED = 0;
    public const EU_SUBJECT_CA_SERVER_SUB_TYPE_CMP = 1;
    public const EU_SUBJECT_CA_SERVER_SUB_TYPE_TSP = 2;
    public const EU_SUBJECT_CA_SERVER_SUB_TYPE_OCSP = 3;
}
