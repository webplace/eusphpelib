<?php

namespace Webplace\EusphpeLib\Enums;

interface SignLevelEnum
{
    public const EU_XADES_SIGN_LEVEL_UNKNOWN = 0;
    public const EU_XADES_SIGN_LEVEL_B_B = 1;
    public const EU_XADES_SIGN_LEVEL_B_T = 4;
    public const EU_XADES_SIGN_LEVEL_B_LT = 16;
    public const EU_XADES_SIGN_LEVEL_B_LTA = 32;
}
