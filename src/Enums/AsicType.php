<?php

namespace Webplace\EusphpeLib\Enums;

interface AsicType
{
    public const EU_ASIC_TYPE_UNKNOWN = 0;
    public const EU_ASIC_TYPE_S = 1;
    public const EU_ASIC_TYPE_E = 2;
}
